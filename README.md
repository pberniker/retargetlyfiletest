### What is this repository for? ###

* Armar una API que pueda devolver métricas obtenidas a partir del procesamiento de un archivo de datos crudo.

### How do I get set up? ###

* Clone "https://pberniker@bitbucket.org/pberniker/retargetlyfiletest.git".
* Execute "npm install".
* Copy ".env.example" to ".env".
* Execute "node index".
* In "Retargetly.postman_collection.json" are the calls to the endpoints in Postam.

### Improvements ###

* Dockerizar.
* Do refactors in todos (Todo).