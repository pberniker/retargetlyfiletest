const fs = require('fs')
const app = require('./modules/express')
const env = require('./modules/env')
const log = require('./modules/log')
const exception = require('./modules/exception')
const header = require('./modules/header')
const jwt = require('./modules/jwt')
const file = require('./modules/file')





app.post('/login', function(req, res) {
    console.clear()
    
    try {
        log.message('post > /login')

        const encodeUserAndPass = header.getAuthorization(req, 'Basic')
        log.variable('encodeUserAndPass', encodeUserAndPass)
        if (!encodeUserAndPass) { throw new exception.InvalidAuthorizationHeaderException() }

        const decodeUserAndPass = Buffer.from(encodeUserAndPass, 'base64').toString('utf-8');
        log.variable('decodeUserAndPass', decodeUserAndPass)

        arr = decodeUserAndPass.split(':')
        log.variable('arr', arr)
        if (arr.length != 2) { throw new exception.InvalidAuthorizationHeaderException() }

        const user = arr[0]
        log.variable('user', user)
        log.variable('USER', env.get('USER'))
        if (user != env.get('USER')) { throw new exception.UnauthorizedException() }

        const pass = arr[1]
        log.variable('pass', pass)
        log.variable('PASS', env.get('PASS'))
        if (pass != env.get('PASS')) { throw new exception.UnauthorizedException() }
        
        const json = jwt.make({ user })
        log.variable('json', json)

        res.send(json)
    }
    catch(ex) {
        // Todo: Refactorizar
        log.variable('ex', ex)
        const status = ex.status || 500        
        if (status == 500) { res.status(500).send({ message: 'Internal server error' }) }
        res.status(status).send({ message: ex.message })
    }
});





app.get('/files/list', function(req, res) {
    console.clear()
    
    function verifyCallBack(ex, tokenData) {
        // Todo: Refactor 
        if (ex) {
            res.status(401).send({ message: ex.message })
            return
        }
        // ---------------
        process()
    }

    function process() {
        let body = []

        const extensionFile = env.get('FILE_EXTENSION').toLowerCase()

        fs.readdir('./files', function(err, items) {         
            for (var j = 0; j < items.length; j ++) {
                const item = items[j];
                const arr = item.split('.')
                const isTsvFile = arr.pop().toLowerCase() == extensionFile
                
                log.variable('item', item)
                log.variable('isTsvFile', isTsvFile)

                if (isTsvFile) {
                    const path = `./files/${item}`
                    const stats = fs.statSync(path)
                    const bytes = stats.size
                    
                    const i = Math.floor(Math.log(bytes) / Math.log(1024));
                    const size = (bytes / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + ['B', 'KB', 'MB', 'GB', 'TB'][i];
                    
                    const fileInfo = { name: item, size }

                    body.push(fileInfo)
                    
                    log.variable('bytes', bytes)
                    log.variable('size', size)
                }

                log.brakLine()
            }

            res.send(body)
        });        
    }
    
    try {
        log.message('get > /files/list', true)
        // Todo: Refactor
        const token = header.getAuthorization(req, 'Bearer')
        log.variable('token', token)
        jwt.verify(token, verifyCallBack)
        // ---------------
    }
    catch(ex) {
        // Todo: Refactorizar
        log.variable('ex', ex)
        const status = ex.status || 500        
        if (status == 500) { res.status(500).send({ message: 'Internal server error' }) }
        res.status(status).send({ message: ex.message })
        // ---------------
    }
});





let fileManager = new file.Manager()

app.get('/files/metrics', function(req, res) {
    function verifyCallBack(ex, tokenData) {
        // Todo: Refactor 
        if (ex) {
            res.status(401).send({ message: ex.message })
            return
        }
        // ---------------
        const file = fileManager.process(req.query.filename)
        res.send(file)
    }

    try {
        log.message('get > /files/metrics', true)
        if (!req.query.filename) { throw new exception.RequiredException('Query', 'filename') }
        const token = header.getAuthorization(req, 'Bearer')
        log.variable('token', token)
        jwt.verify(token, verifyCallBack)
    }
    catch(ex) {
        // Todo: Refactorizar
        log.variable('ex', ex)
        const status = ex.status || 500        
        if (status == 500) { res.status(500).send({ message: 'Internal server error' }) }
        res.status(status).send({ message: ex.message })
        // ---------------
    }
});