const fs = require('fs');
const lineReader = require('line-reader');
const log = require('../modules/log')

class Manager {  
    constructor () {
        this.files = []
    }

    process(filename) {
        let files = this.files
        
        let file = getFile(files, filename)

        if (file != null) { return getResFile(file) }

        file = start(filename, this.files)

        const path = `./files/${filename}`

        fs.access(path, (err) => {
            if (err) {
                error(file, err)
                return
            } 

            lineReader.eachLine(path, function(line, last, cb) {
                console.clear()
                log.variable('last', last)
                
                processing(file, line)

                if (last) { 
                    ready(file)
                    cb(false)
                    return
                } 
    
                cb()
            })
        });

        return file
    } 
}

// Todo: Hacer que sean metodos privados de la clase.

function getFile(files, filename) { 
    for (var j = 0; j < files.length; j ++) {
        if (filename.toLowerCase() == files[j].filename.toLowerCase()) { return files[j] }
        return null
    }
}

function start(filename, files) {
    let file = {
        filename,
        status: 'Started',
        started: new Date()
    }

    files.push(file)
    
    return file
}

function processing(file, lineContent) {
    file.status = 'Processing'
    file.updated = new Date()
    
    const arr = lineContent.split('\t')

    const userId = arr[0]
    const segments = arr[1].split(',')
    const country = arr[2]
    const line = { userId, segments, country }
    
    if (file.lines) {
        file.lines.push(line)
    }
    else {
        let lines = []
        lines.push(line)
        file.lines = lines
    }
}

function error(file, err) {
    file.status = 'Failed'
    file.error = 'Missing file'
    file.updated = new Date()
}

function ready(file) {
    file.metrics = getMetrics(file)
    file.status = 'Ready'
    file.finished = new Date()
    log.variable('file', file)
}

function getMetrics(file) {
    let arr = []

    for (let j = 0; j < file.lines.length; j ++) {
        for (let k = 0; k < file.lines[j].segments.length; k ++) {
            const item = { segment: file.lines[j].segments[k], country: file.lines[j].country }
            arr.push(item)
        }        
    }

    arr.sort(function(a, b) { return a['segment'] - b['segment'] || a['country'] - b['country'] })

    let res = [];
    let j = 0

    while (j < arr.length) {
        prevSegment = arr[j].segment
        let uniques = []

        while (j < arr.length && arr[j].segment == prevSegment) {
            prevCountry = arr[j].country
            let k = 0

            while (j < arr.length && arr[j].segment == prevSegment && arr[j].country == prevCountry) {
                k ++
                j ++
            }

            uniques.push({ country: prevCountry, count: k })
        }

        res.push({ segment: prevSegment, uniques })
    }

    return res
}

function getResFile(file) {
    const res = { 
        filename: file.filename,
        status: file.status,
        started: file.started
    }

    if (file.status == 'Ready') {
        res['finished'] = file.finished
        res['metrics'] = file.metrics
        return res
    } 

    if (file.status == 'Processing') {
        res['updated'] = file.updated
        return res        
    }

    if (file.status == 'Ready') {
        res['finished'] = file.finished
        res['metrics'] = file.metrics
        return res
    }

    if (file.status == 'Error') {
        res['updated'] = file.updated
        res['error'] = file.error
        return res
    }

    return res
}

module.exports.Manager = Manager