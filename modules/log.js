function message(message, addBrakLine = false) { 
    console.log(message) 
    if (addBrakLine) { brakLine() } 
}

function variable(name, variable, addBrakLine = false) { 
    console.log(`${name}:`, variable) 
    if (addBrakLine) { brakLine() } 
}

function brakLine() {
    console.log()
}

module.exports = { message, variable, brakLine }