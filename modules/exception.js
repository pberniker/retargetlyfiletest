class ValidationException extends Error {  
    constructor (message, code) {
        super(message)
        this.name = this.constructor.name
        this.status = code
    }
}

class RequiredException extends ValidationException {  
    constructor (reqType, variableName) {
        super(`${reqType} "${variableName}" is required`, 400)
    }
}

class InvalidException extends ValidationException {  
    constructor (reqType, variableName) {
        super(`${reqType} "${variableName}" is invalid`, 400)
    }
}

class InvalidAuthorizationHeaderException extends InvalidException {  
    constructor () {
        super('Header', 'Authorization')
    }
}

class UnauthorizedException extends ValidationException {  
    constructor () {
        super('Unauthorized', 401)
    }
}

class FileNotExistsException extends ValidationException {  
    constructor () {
        super('File not exists', 500)
    }
}

module.exports = { ValidationException, RequiredException, InvalidException, InvalidAuthorizationHeaderException, UnauthorizedException, FileNotExistsException }