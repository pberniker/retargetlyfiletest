const log = require('./log');
const exception = require('./exception');

function getAuthorization(req, type) {
    const headers = req.headers

    log.variable('headers', headers)

    if (!headers.authorization) { throw new exception.RequiredException('Header', 'Authorization') }

    const authorization = headers.authorization
    log.variable('authorization', authorization)

    if (!authorization) { throw new exception.RequiredException('Header', 'Authorization') }
    
    const i = authorization.toLowerCase().indexOf(type.toLowerCase());
    log.variable('i', i)

    if (i == -1) { throw new exception.InvalidAuthorizationHeaderException() }
    
    let arr = authorization.split(' ')
    log.variable('arr', arr)

    if (arr.length != 2) { throw new exception.InvalidAuthorizationHeaderException() }

    const res = arr[1]
    if (!res) { throw new exception.InvalidAuthorizationHeaderException() }

    return res
}
  
module.exports.getAuthorization = getAuthorization