const dotenv = require('dotenv')

dotenv.config();

function get(name, defaultValue = null) {
    return process.env[name] || defaultValue;
}

module.exports.get = get