const express = require('express')
const bodyParser = require('body-parser')
const env = require('./env');

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));

const port = env.get('PORT')

app.listen(port, function() {
    console.log(`Listening in port ${port} ...`)
})

module.exports = app