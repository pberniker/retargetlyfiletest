const jwt = require('jsonwebtoken')
const env = require('./env')

function make(data) {
    const expiresInMinutes = env.get('TOKEN_EXPIRES_IN_MINUTES')
    const privateKey = env.get('TOKEN_PRIVATE_KEY')
    const token = jwt.sign(data, privateKey, { expiresIn: expiresInMinutes * 60 })
    const expiresIn = `+ ${expiresInMinutes} minute(s)`
    const expiresAt = getExpiresAt(expiresInMinutes)
    const res = { token, expiresIn, expiresAt }
    return res
}
  
function verify(token, callBack) {
    const privateKey = env.get('TOKEN_PRIVATE_KEY')
    jwt.verify(token, privateKey, callBack)
}

function getExpiresAt(expiresInMinutes) {
    let dte = new Date();
    dte.setMinutes(expiresInMinutes)
    const iso = dte.toISOString()
    const arr = iso.split('.')
    const res = arr[0]
    return res
}

module.exports = { make, verify }